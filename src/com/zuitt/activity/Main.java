package com.zuitt.activity;

public class Main {

    public Main(){}

    public static void main(String[] args) {

        Phonebook myPhonebook = new Phonebook();

        Contact myContact = new Contact();
        myContact.setName("Jane Doe");
        myContact.setContactNumber("09191234567");
        myContact.setAddress("Earth");



        Contact myContact2 = new Contact();
        myContact2.setName("John Doe");
        myContact2.setContactNumber("091977788888");
        myContact2.setAddress("Mars");
        myPhonebook.setContacts(myContact);
        myPhonebook.setContacts(myContact2);



        if(myPhonebook.getContacts().isEmpty()){
            System.out.println("The phonebook is currently empty.");
        }else{
            //for each contact in the phonebook, invoke the printInfo() method of the Main class
            for(Contact contact: myPhonebook.getContacts()){
                System.out.println("Name: " + contact.getName());
                System.out.println("Contact No.: " + contact.getContactNumber());
                System.out.println("Address: " + contact.getAddress() + "\n");

            }

//            myPhonebook.getContacts().forEach((contact) -> printInfo(contact));
        }
    }

    //method for printing out information for every contact in the phonebook
//    public static void printInfo(Contact contact){
//        System.out.println(contact.getName());
//        System.out.println("-------------------");
//        if(contact.getContactNumber().isEmpty()){
//            System.out.println(contact.getName() + " has no registered number.");
//        }else{
//            System.out.println(contact.getName() + " has the following registered number:");
//            System.out.println(contact.getContactNumber());
//        }
//        if(contact.getAddress().isEmpty()){
//            System.out.println(contact.getName() + " has no registered address.");
//        }else{
//            System.out.println(contact.getName() + " has the following registered address:");
//            System.out.println(contact.getAddress() + "\n");
//        }
//    }





}
