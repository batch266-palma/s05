package com.zuitt.activity;

import java.util.ArrayList;
import java.util.Arrays;

public class Phonebook {
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    public Phonebook(){}

    public Phonebook(Contact contact){
        this.contacts.add(contact);
    }


    public ArrayList<Contact> getContacts() {

        return contacts;
    }

    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }

}
